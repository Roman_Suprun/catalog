/****** Object:  Table [dbo].[Groups]    Script Date: 09.10.2017 20:05:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Groups](
	[GroupId] [uniqueidentifier] NOT NULL,
	[GroupTitle] [nvarchar](160) NOT NULL,
	[GroupDescription] [nvarchar](max) NULL,
	[GroupKey] [nvarchar](160) NOT NULL,
 CONSTRAINT [PK_Groups] PRIMARY KEY NONCLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 09.10.2017 20:05:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [uniqueidentifier] NOT NULL,
	[ProductHistoryId] [uniqueidentifier] NOT NULL,
	[GroupId] [uniqueidentifier] NOT NULL,
	[ProductKey] [nvarchar](160) NOT NULL,
	[ProductTitle] [nvarchar](160) NOT NULL,
	[ProductDescription] [nvarchar](max) NULL,
	[ProductCoast] [money] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY NONCLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductsHistory]    Script Date: 09.10.2017 20:05:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsHistory](
	[ProductHistoryId] [uniqueidentifier] NOT NULL,
	[GroupId] [uniqueidentifier] NOT NULL,
	[ProductId] [uniqueidentifier] NOT NULL,
	[StatusId] [int] NOT NULL,
	[EditorUserId] [int] NOT NULL,
	[AproverUserId] [int] NOT NULL,
	[ProductTitle] [nvarchar](160) NOT NULL,
	[ProductDescription] [nvarchar](max) NULL,
	[ProductCost] [money] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[EditDate] [datetime] NOT NULL,
	[AprovingDate] [datetime] NOT NULL,
	[ProductKey] [nvarchar](160) NOT NULL,
 CONSTRAINT [PK_ProductsHistory] PRIMARY KEY NONCLUSTERED 
(
	[ProductHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Statuses]    Script Date: 09.10.2017 20:05:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Statuses](
	[StatusId] [int] NOT NULL,
	[StatusTitle] [nvarchar](160) NOT NULL,
 CONSTRAINT [PK_Statuses] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 09.10.2017 20:05:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserKey] [nvarchar](160) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Groups] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Groups] ([GroupId])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Groups]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_ProductsHistory] FOREIGN KEY([ProductHistoryId])
REFERENCES [dbo].[ProductsHistory] ([ProductHistoryId])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_ProductsHistory]
GO
ALTER TABLE [dbo].[ProductsHistory]  WITH CHECK ADD  CONSTRAINT [FK_ProductsHistory_Groups] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Groups] ([GroupId])
GO
ALTER TABLE [dbo].[ProductsHistory] CHECK CONSTRAINT [FK_ProductsHistory_Groups]
GO
ALTER TABLE [dbo].[ProductsHistory]  WITH CHECK ADD  CONSTRAINT [FK_ProductsHistory_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[ProductsHistory] CHECK CONSTRAINT [FK_ProductsHistory_Products]
GO
ALTER TABLE [dbo].[ProductsHistory]  WITH CHECK ADD  CONSTRAINT [FK_ProductsHistory_Statuses] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Statuses] ([StatusId])
GO
ALTER TABLE [dbo].[ProductsHistory] CHECK CONSTRAINT [FK_ProductsHistory_Statuses]
GO
ALTER TABLE [dbo].[ProductsHistory]  WITH CHECK ADD  CONSTRAINT [FK_ProductsHistory_Users] FOREIGN KEY([EditorUserId])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[ProductsHistory] CHECK CONSTRAINT [FK_ProductsHistory_Users]
GO
ALTER TABLE [dbo].[ProductsHistory]  WITH CHECK ADD  CONSTRAINT [FK_ProductsHistory_Users1] FOREIGN KEY([EditorUserId])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[ProductsHistory] CHECK CONSTRAINT [FK_ProductsHistory_Users1]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-Created; 2-Needly to aproved; 3-Aproved; 4-Old' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProductsHistory', @level2type=N'COLUMN',@level2name=N'StatusId'
GO

SET IDENTITY_INSERT [dbo].[Statuses] ON
GO
INSERT INTO [dbo].[Statuses] (StatusId, StatusTitle) Values (1, 'Created'), (2, 'ReadyToApprove') ,(3, 'Approved') ,(4, 'Old')  ;
GO
SET IDENTITY_INSERT [dbo].[Statuses] OFF 


